using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Apertura9 : MonoBehaviour
{
    public Animator Door;

    private void OnTriggerEnter(Collider other)
    {
        Door.Play("openDoor9");
    }

    private void OnTriggerExit(Collider other)
    {
        Door.Play("closeDoor9");
    }
}