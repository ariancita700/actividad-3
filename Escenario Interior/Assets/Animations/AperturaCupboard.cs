using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AperturaCupboard : MonoBehaviour
{
    public Animator Door;

    private void OnTriggerEnter(Collider other)
    {
        Door.Play("openDoorCupboard");
    }

    private void OnTriggerExit(Collider other)
    {
        Door.Play("closeDoorCupboard");
    }
}
