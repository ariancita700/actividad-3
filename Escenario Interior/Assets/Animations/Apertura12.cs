using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Apertura12 : MonoBehaviour
{
    public Animator Door;

    private void OnTriggerEnter(Collider other)
    {
        Door.Play("openDoor12");
    }

    private void OnTriggerExit(Collider other)
    {
        Door.Play("closeDoor12");
    }
}
