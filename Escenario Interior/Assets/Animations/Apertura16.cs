using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Apertura16 : MonoBehaviour
{
    public Animator Door;

    private void OnTriggerEnter(Collider other)
    {
        Door.Play("openDoor16");
    }

    private void OnTriggerExit(Collider other)
    {
        Door.Play("closeDoor16");
    }
}
