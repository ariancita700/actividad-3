using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Apertura4 : MonoBehaviour
{
    public Animator Door;

    private void OnTriggerEnter(Collider other)
    {
        Door.Play("openDoor4");
    }

    private void OnTriggerExit(Collider other)
    {
        Door.Play("closeDoor4");
    }
}
