using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AperturaCloset : MonoBehaviour
{
    public Animator Door;

    private void OnTriggerEnter(Collider other)
    {
        Door.Play("openDoorCloset");
    }

    private void OnTriggerExit(Collider other)
    {
        Door.Play("closeDoorCloset");
    }
}
