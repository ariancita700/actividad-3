using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Apertura7 : MonoBehaviour
{
    public Animator Door;

    private void OnTriggerEnter(Collider other)
    {
        Door.Play("openDoor7");
    }

    private void OnTriggerExit(Collider other)
    {
        Door.Play("closeDoor7");
    }
}
