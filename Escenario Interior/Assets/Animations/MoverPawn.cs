using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoverPawn : MonoBehaviour
{
    public Animator Door;

    private void OnTriggerEnter(Collider other)
    {
        Door.Play("movePawn");
    }

    private void OnTriggerExit(Collider other)
    {
        Door.Play("movePawnBack");
    }
}
